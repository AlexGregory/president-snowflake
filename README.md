# README #

UAT Game Jam Game - Theme: "Triggered/Safe Space"

### CONCEPT ###

* As a Secret-Secret Service Agent, your job is to throw yourself in harm's way to protect President Snowflake from his most dreaded enemy -- dissenting opinions.
* Version: 0.1a

### TOOLS ###

* Unity - C#

### Who do I talk to? ###

* Hue Henry - mhenry@uat.edu
