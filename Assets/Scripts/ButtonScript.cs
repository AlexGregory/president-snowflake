﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonScript : MonoBehaviour {
	public Text musicText;
	public Text sfxText;
	public GameObject comicSpawn;
	public GameObject comicPrefab;
	public GameObject Canvas;
	// Use this for initialization
	void Start ()
	{
		if (musicText != null) {
			musicText.text = "Music: " + (AudioManager._AUDIOMANAGER.musicVolume  * 100).ToString ();
			sfxText.text = "SFX: " + (AudioManager._AUDIOMANAGER.soundFXVolume* 100).ToString();
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void IncreaseMusicVolume()
	{
		if (AudioManager._AUDIOMANAGER.musicVolume < 1) 
		{
			AudioManager._AUDIOMANAGER.musicVolume += .05f;
			musicText.text = "Music: " + (AudioManager._AUDIOMANAGER.musicVolume  * 100).ToString();
			AudioManager._AUDIOMANAGER.GetComponent<AudioSource> ().volume = AudioManager._AUDIOMANAGER.musicVolume;
		}
	}

	public void DecreaseMusicVolume()
	{
		if (AudioManager._AUDIOMANAGER.musicVolume > 0) 
		{
			AudioManager._AUDIOMANAGER.musicVolume -= .05f;
			musicText.text = "Music: " + (AudioManager._AUDIOMANAGER.musicVolume  * 100).ToString();
			AudioManager._AUDIOMANAGER.GetComponent<AudioSource> ().volume = AudioManager._AUDIOMANAGER.musicVolume;
		}
	}

	public void IncreasFXVolume()
	{
		if (AudioManager._AUDIOMANAGER.soundFXVolume < 1) 
		{
			AudioManager._AUDIOMANAGER.soundFXVolume += .05f;
			sfxText.text = "SFX: " + (AudioManager._AUDIOMANAGER.soundFXVolume  * 100).ToString();
			AudioManager._AUDIOMANAGER.audioSource.volume = AudioManager._AUDIOMANAGER.soundFXVolume;
		}
	}

	public void DecreaseFXVolume()
	{
		if (AudioManager._AUDIOMANAGER.soundFXVolume > 0) 
		{
			AudioManager._AUDIOMANAGER.soundFXVolume -= .05f;
						sfxText.text = "SFX: " + (AudioManager._AUDIOMANAGER.soundFXVolume  * 100).ToString();
			AudioManager._AUDIOMANAGER.audioSource.volume = AudioManager._AUDIOMANAGER.soundFXVolume;
		}
	}

	public void toSoundScene()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene (1);
	}
	public void toMainMenuScene()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene (0);
	}
	public void toEndScreen()
	{
		GameManager._GAMEMANAGER.gameObject.SetActive (false);
		UnityEngine.SceneManagement.SceneManager.LoadScene (2);
	}
	public void toMainGame()
	{
		comicSpawn = Instantiate (comicPrefab, comicSpawn.GetComponent<Transform> ().position, Quaternion.identity);
		Canvas.SetActive (false);
	}

	public void toCredits()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene (4);
	}
}
