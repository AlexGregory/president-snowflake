﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class TweetData { 
	[Header("Data")]
	public Sprite icon;
	public string handle;
	public string timeTweeted;
	public string textContent;
}


public class Tweet : MonoBehaviour {

	[Header("Data")]
	public TweetData data;

	[Header("UI Elements")]
	public Image iconImage;
	public Text tweetTextbox;
	public Text headerTextbox;

	public void UpdateTweet () {
			iconImage.sprite = data.icon;
			headerTextbox.text = data.handle + " - " + data.timeTweeted;
			tweetTextbox.text = data.textContent;
	}

	// Update is called once per frame
	void Start () {
		UpdateTweet ();
	}
}
