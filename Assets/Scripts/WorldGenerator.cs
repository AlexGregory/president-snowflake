﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGenerator : MonoBehaviour {

	public GameObject presidentStorage;
	public GameObject buildingPrefab;
	public GameObject temp;
	public float currentBuildingX;
	public float distanceOutBuild = 24.0f;
	public float buildingY;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (presidentStorage.GetComponent<Transform> ().position.x >= (currentBuildingX - 40)) 
		{

			temp = Instantiate(buildingPrefab,new Vector3(currentBuildingX + distanceOutBuild, buildingY, 0),  Quaternion.identity) as GameObject;
			temp.GetComponent<House>().President = presidentStorage;
			currentBuildingX += 3;
		}
	}
}
