﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Action  {

	public enum ActionType { Test, ShoutRobot, ShoutAlien, ShoutRaygun, SafeRobot, SafeAlien, SafeRaygun, PushAll };
	public ActionType actionToPerform;

	// public float cooldownTime;
	public float minLevelReq; // can't get this action until we reach this level
	public Sprite icon; // Icon that shows up that you have this action

	public GameObject  temp;

	public void DoAction (){
		if (!GameManager._GAMEMANAGER.IsOnGlobalCooldown()) { 		

			switch (actionToPerform) {
			case ActionType.Test:
				DoActionSayHello ();
				break;
			case ActionType.ShoutAlien:
				DoShoutAlien ();
				break;
			case ActionType.ShoutRaygun:
				DoShoutRayguns ();
				break;
			case ActionType.ShoutRobot:
				DoShoutRobots () ;
				break;
			case ActionType.SafeAlien:
				DoActionAlienSafespace ();
				break;
			case ActionType.SafeRaygun:
				DoActionRaygunSafespace ();
				break;
			case ActionType.SafeRobot:
				DoActionRobotSafespace ();
				break;
			case ActionType.PushAll:
				DoShoutPushAll();
				break;
			}

			// Start the cooldown
			GameManager._GAMEMANAGER.StartGlobalCooldown ();
		}
	}

	public void DoShoutAlien () 
	{

		Debug.Log ("Aliens sure are swell!");
		GameObject.Instantiate (GameManager._GAMEMANAGER.alienBubble, GameManager._GAMEMANAGER.playerBubbleSpawn.GetComponent<Transform> ().position, Quaternion.identity,GameManager._GAMEMANAGER.playerBubbleSpawn.GetComponent<Transform>());

			AudioManager._AUDIOMANAGER.playSound ("AlienShout");
		
		// For every protester
		foreach (ProtesterController protester in GameManager._GAMEMANAGER.protesters) {
			// If they are in range, 
			if (Vector3.Distance (protester.data.tf.position, GameManager._GAMEMANAGER.player.tf.position) <= GameManager._GAMEMANAGER.shoutRange) {
				// Check if they are anti-alien
				if (protester.topic.topic == Topic.Topics.Aliens && !protester.topic.isProTopic) {
					//TODO: Play their angry face
					Debug.Log("Enrage: "+protester.name+" is angry.");

					// Set their seek player timer 
					protester.chasePlayerTimeRemaining = GameManager._GAMEMANAGER.chaseTime;
				}
			}
		}
	}

	public void DoShoutRobots () 
	{

		Debug.Log ("Robots sure are swell!");

		GameObject.Instantiate (GameManager._GAMEMANAGER.robotBubble, GameManager._GAMEMANAGER.playerBubbleSpawn.GetComponent<Transform> ().position, Quaternion.identity,GameManager._GAMEMANAGER.playerBubbleSpawn.GetComponent<Transform>());

			AudioManager._AUDIOMANAGER.playSound ("RobotShout");


		// For every protester
		foreach (ProtesterController protester in GameManager._GAMEMANAGER.protesters) {
			// If they are in range, 
			if (Vector3.Distance (protester.data.tf.position, GameManager._GAMEMANAGER.player.tf.position) <= GameManager._GAMEMANAGER.shoutRange) {
				// Check if they are anti-robot
				if (protester.topic.topic == Topic.Topics.Robots && !protester.topic.isProTopic) {
					//TODO: Play their angry face
					Debug.Log("Enrage: "+protester.name+" is angry.");

					// Set their seek player timer 
					protester.chasePlayerTimeRemaining = GameManager._GAMEMANAGER.chaseTime;
				}
			}
		}
	}

	public void DoShoutRayguns () 
	{

		Debug.Log ("Rayguns sure are swell!");

		GameObject.Instantiate (GameManager._GAMEMANAGER.raygunBubble, GameManager._GAMEMANAGER.playerBubbleSpawn.GetComponent<Transform> ().position, Quaternion.identity,GameManager._GAMEMANAGER.playerBubbleSpawn.GetComponent<Transform>());

			AudioManager._AUDIOMANAGER.playSound ("RaygunShout");


		// For every protester
		foreach (ProtesterController protester in GameManager._GAMEMANAGER.protesters) {
			// If they are in range, 
			if (Vector3.Distance (protester.data.tf.position, GameManager._GAMEMANAGER.player.tf.position) <= GameManager._GAMEMANAGER.shoutRange) {
				// Check if they are anti-raygun
				if (protester.topic.topic == Topic.Topics.Rayguns && !protester.topic.isProTopic) {
					//TODO: Play their angry face
					Debug.Log("Enrage: "+protester.name+" is angry.");

					// Set their seek player timer 
					protester.chasePlayerTimeRemaining = GameManager._GAMEMANAGER.chaseTime;
				}
			}
		}
	}

	public void DoShoutPushAll () 
	{

		//Debug.Log ("Everyone go away!");

		AudioManager._AUDIOMANAGER.playSound ("AllShout");

		// For every protester
		foreach (ProtesterController protester in GameManager._GAMEMANAGER.protesters) 
		{
			
			// If they are in range, 
			if (Vector3.Distance (protester.data.tf.position, GameManager._GAMEMANAGER.player.tf.position) <= GameManager._GAMEMANAGER.shoutRange) 
			{
				Vector3 vectorToTarget = protester.data.tf.position - GameManager._GAMEMANAGER.player.tf.position;
				vectorToTarget.Normalize ();
				vectorToTarget *= GameManager._GAMEMANAGER.pushDistance;

				protester.SetTarget (protester.data.tf.position + vectorToTarget, GameManager._GAMEMANAGER.endDelayPushAll);
					//TODO: Play their angry face
					//Debug.Log("Enrage: "+protester.name+" is fleeing.");

					// Set their seek player timer 
					
			}
		}

	}

	public void DoActionRobotSafespace()
	{
		AudioManager._AUDIOMANAGER.playSound ("SafeSpaceShout");
		temp = GameObject.Instantiate (GameManager._GAMEMANAGER.robotSafeSpace, GameManager._GAMEMANAGER.player.tf.position, Quaternion.identity) as GameObject;
		temp.GetComponent<SafeSpace> ().lifeTimer = GameManager._GAMEMANAGER.DroppedSafeSpaceDuration;
	}

	public void DoActionAlienSafespace()
	{

		AudioManager._AUDIOMANAGER.playSound ("SafeSpaceShout");
		temp = GameObject.Instantiate(GameManager._GAMEMANAGER.alienSafeSpace, GameManager._GAMEMANAGER.player.tf.position, Quaternion.identity) as GameObject;
		temp.GetComponent<SafeSpace> ().lifeTimer = GameManager._GAMEMANAGER.DroppedSafeSpaceDuration;
	}

	public void DoActionRaygunSafespace()
	{

		AudioManager._AUDIOMANAGER.playSound ("SafeSpaceShout");
		temp = GameObject.Instantiate(GameManager._GAMEMANAGER.raygunSafeSpace, GameManager._GAMEMANAGER.player.tf.position, Quaternion.identity) as GameObject;
		temp.GetComponent<SafeSpace> ().lifeTimer = GameManager._GAMEMANAGER.DroppedSafeSpaceDuration;
	}

	public void DoActionSayHello () {
		// Do something
		Debug.Log ("HELLO!");
	}
}