﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionDeck : MonoBehaviour {

	[Header("DESIGNERS CHANGE HERE")]
	public List<Action> fullDeck;

	[Header("DO NOT CHANGE")]
	public List<Action> currentDeck;


	// Use this for initialization
	void Start () {
		ShuffleDeck (true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShuffleDeck(bool reset = true) {
		if (reset) {
			currentDeck = new List<Action> (fullDeck);
		}

		for (int i = 0; i < currentDeck.Count*10; i++) {
			int randomCard = Random.Range (0, currentDeck.Count);
			currentDeck.Add (currentDeck [randomCard]);
			currentDeck.RemoveAt (randomCard);
		}
	}

	public Action DrawTopCard () {
		int timeout = 0;

		// if deck is empty, shuffle
		if (currentDeck.Count <= 0) {
			ShuffleDeck (true);
		}

		// Draw the bottom card
		Action topAction = currentDeck [0];

		/** TODO: If it is the too difficult difficulty, discard it.
		while (topAction.minLevelReq >= GameManager._GAMEMANAGER.difficultyLevel) {
			// Discard it
			currentDeck.RemoveAt(0);

			// if deck is empty, shuffle
			if (currentDeck.Count < 0) {
				ShuffleDeck (true);
			}

			// Draw another card
			topAction = currentDeck [0];

			// If we've run the deck 2 times and can't find a card - quit trying and return null
			timeout++; 
			if (timeout >= fullDeck.Count*2) {
				return null;
			}
		} **/

		// Otherwise, we found a suitable card, so remove and return it
		currentDeck.RemoveAt (0);
		return topAction;
	}

	public Action DrawBottomCard () {
		int timeout = 0;

		// if deck is empty, shuffle
		if (currentDeck.Count <= 0) {
			ShuffleDeck (true);
		}

		// Draw the bottom card
		Action topAction = currentDeck [currentDeck.Count-1];

		/** TODO: If it is the too difficult difficulty, discard it.
		while (topAction.minLevelReq >= GameManager._GAMEMANAGER.difficultyLevel) {
			// Discard it
			currentDeck.RemoveAt(currentDeck.Count-1);

			// if deck is empty, shuffle
			if (currentDeck.Count <= 0) {
				ShuffleDeck (true);
			}

			// Draw another card
			topAction = currentDeck [currentDeck.Count-1];

			// If we've run the deck 2 times and can't find a card - quit trying and return null
			timeout++; 
			if (timeout >= fullDeck.Count*2) {
				return null;
			}
		} **/

		// Otherwise, we found a suitable card, so remove and return it
		currentDeck.RemoveAt (currentDeck.Count-1);
		return topAction;
	}

	public Action DrawRandomCard ( ) {
		int timeout = 0;

		// if deck is empty, shuffle
		if (currentDeck.Count <= 0) {
			ShuffleDeck ();
		}

		int randomCard = Random.Range (0, currentDeck.Count);
		Action topAction = currentDeck [randomCard];

		/*** TODO: If it is the too difficult difficulty, discard it.
		while (topAction.minLevelReq >= GameManager._GAMEMANAGER.difficultyLevel) {
			// Discard it
			currentDeck.RemoveAt(randomCard);

			// if deck is empty, shuffle
			if (currentDeck.Count <= 0) {
				ShuffleDeck (true);
			}

			// Draw another card
			topAction = currentDeck [randomCard];

			// If we've run the deck 2 times and can't find a card - quit trying and return null
			timeout++; 
			if (timeout >= fullDeck.Count*2) {
				return null;
			}
		} **/

		// Otherwise, we found a suitable card, so move the bottom card to its position and return our random card
		currentDeck [randomCard] = currentDeck [currentDeck.Count - 1];
		currentDeck.RemoveAt (currentDeck.Count - 1);
		return topAction;


	}

}
