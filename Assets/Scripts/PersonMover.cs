﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonMover : MonoBehaviour {

	public CharacterController cc;
	public Animator animator;
	public Transform tf;

	// Use this for initialization
	void Awake () {
		cc = GetComponent<CharacterController> ();
		animator = GetComponent<Animator> ();
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Move(Vector3 moveVector) {

		// Actually Move
		cc.Move(moveVector);

		// y is z
		Vector3 temp = tf.position;
		temp.z = temp.y * 0.001f;
		tf.position = temp;

		// Set animator
		if (Mathf.Abs(moveVector.x) < GameManager._GAMEMANAGER.animMoveCutoff ) {
			animator.SetTrigger ("Idle");
			animator.SetFloat ("XVelocity",0 );
		} else {
			animator.SetFloat ("XVelocity", moveVector.x);
		}
	}

}
