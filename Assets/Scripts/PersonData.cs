﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonData : MonoBehaviour {

	[Header("Components")]
	public PersonMover mover;
	public Transform tf;
	public Animator animator;


	[Header("Wander Data")]
	[Range(0,1)] public float wanderChance; // Percent chance that they will wander when they do
	public float wanderDecisionTime; // How many times per second do they choose if they want to wander
	public float wanderSpeed; // If they wander, how fast do the wander there.
	public float minTimeBetweenWanders; // If they choose to wander, how long until they can choose to wander again.
	[Range(0,3)] public int wanderDistance; // number of lanes they could wander (Y)
	public float wanderDistanceX; // How far on the X axis might they wander

	public Vector3 targetPosition;
	public int targetLane;
	public float targetXDelta;

	// Use this for initialization
	void Awake () {
		tf = GetComponent<Transform>();
		mover = GetComponent<PersonMover> ();		
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {

	}
}
